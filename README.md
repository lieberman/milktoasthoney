# MilkToastHoney

1. Entrar na pasta master e executar o script `create-networks.sh` para criar as redes.
2. Executar o docker-compose da pasta master para levantar os serviços de: Lokinet, VPN e Proxy
3. Entrar na pasta Matrix e levantar o serviço (provavelmente será necessário baixar e levantar uma vez por causa das configurações geradas)
4. Entrar na pasta jitsi e levantar o serviço